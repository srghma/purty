module Helpers where

import "rio" RIO

import qualified "purescript" Language.PureScript.CST

foldMapWithNext :: Monoid m => (a -> Maybe a -> m) -> [a] -> m
foldMapWithNext _ []            = mempty
foldMapWithNext f [x]           = f x Nothing
foldMapWithNext f (x : x' : xs) = f x (Just x') <> foldMapWithNext f (x' : xs)

getIdentFromValueBindingFields :: Language.PureScript.CST.ValueBindingFields a -> Language.PureScript.CST.Ident
getIdentFromValueBindingFields (Language.PureScript.CST.ValueBindingFields (Language.PureScript.CST.Name _ ident) _ _) = ident

valueBindingFieldsHaveSameIdent :: Language.PureScript.CST.ValueBindingFields a -> Language.PureScript.CST.ValueBindingFields a -> Bool
valueBindingFieldsHaveSameIdent a b = getIdentFromValueBindingFields a == getIdentFromValueBindingFields b
